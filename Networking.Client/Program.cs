﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Networking.Core;

namespace Networking.Client
{
	class Program
	{
		static void Main(string[] args)
		{
			string hostname = "127.0.0.1";
			int port = 4444;

			Client client = new Client(hostname, port);

			bool connected = client.ConnectToHost();

			if(connected)
			{
				try
				{
					string message = "";
					while (message != "/disconnect")
					{
						message = NetworkingUtils.RecieveStringFromClient(client.NetworkStream);
						NetworkingUtils.SendStringToClient(Console.ReadLine(), client.NetworkStream);
					}

				}
				catch (Exception ex)
				{
					Console.WriteLine("Unexpected Error");
					Console.WriteLine(ex.ToString());
					return;
				}
			}

			client.DisconnectFromHost();

			Console.ReadLine();
		}
	}
}

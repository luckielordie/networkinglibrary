﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Core.Server
{
	public class BaseServer
	{
		protected TcpListener tcpListener;
		protected Socket clientSocket;
		protected NetworkStream networkStream;
		protected StreamReader streamReader;
		protected StreamWriter streamWriter;

		protected int listenPort = 4444;
		protected string listenIp = "127.0.0.1";

		public BaseServer(string listenIp, int listenPort)
		{
			this.listenPort = listenPort;
			this.listenIp = listenIp;
			IPAddress ip = IPAddress.Parse(this.listenIp);
			this.tcpListener = new TcpListener(ip, this.listenPort);	
		}

		public void Listen()
		{
			this.tcpListener.Start();
			Console.WriteLine("Server Listening for connection on {0}:{1}", this.listenIp, this.listenPort);

			this.clientSocket = this.tcpListener.AcceptSocket();
			Console.WriteLine("Client Connected");
		}

		public abstract void Run(object socket);

		public void CloseServer()
		{
			if(this.streamReader != null) this.streamReader.Close();
			if(this.streamWriter != null) this.streamWriter.Close();
			if(this.networkStream!= null) this.networkStream.Close();
			if(this.clientSocket != null) this.clientSocket.Close();
		}
	}
}

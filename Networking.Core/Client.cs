﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Networking.Core 
	public class Client
	{
		public string Hostname { get; private set; }
		public int Port { get; private set; }
		public NetworkStream NetworkStream { get { return this.networkStream; } private set; }

		protected TcpClient tcpClient;
		protected NetworkStream networkStream;

		public Client(string connectIP, int connectPort)
		{
			this.Hostname = connectIP;
			this.Port = connectPort;
		}

		public bool ConnectToHost()
		{
			if (this.Hostname == null)
			{
				Console.WriteLine("Configure host address and port");
				return false;
			}

			// try connect
			try
			{
				Console.WriteLine("Connecting to {0}:{1}", this.Hostname, this.Port);
				tcpClient = new TcpClient(this.Hostname, this.Port);
				networkStream = tcpClient.GetStream();

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				Console.WriteLine("Connection Unsuccessful");
				return false;
			}

			Console.WriteLine("Connection Successful!");
			return true;
		}

		public void DisconnectFromHost()
		{
			if(this.networkStream != null) this.networkStream.Close();
			if(this.tcpClient != null) this.tcpClient.Close();
		}
	}
}

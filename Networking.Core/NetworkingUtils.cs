﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Core
{
	public static class NetworkingUtils
	{
		public static void SendStringToClient(string message, NetworkStream networkStream)
		{
			StreamWriter streamWriter = new StreamWriter(networkStream);
			streamWriter.WriteLine(message);
			streamWriter.Flush();			
		}

		public static string RecieveStringFromClient(NetworkStream networkStream)
		{
			StreamReader streamReader = new StreamReader(networkStream);

			string buffer = streamReader.ReadLine();
			Console.WriteLine(buffer);
			return buffer;
			
		}
	}
}

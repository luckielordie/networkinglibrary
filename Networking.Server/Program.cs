﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Networking.Core.Server;

namespace Networking.Server
{
	class Program
	{
		static void Main(string[] args)
		{
			StringServer server = new StringServer("127.0.0.1", 4444);

			server.Listen();
			server.CloseServer();
		}
	}
}
